﻿app.factory('categoryService', ['$http',
function ($http) {

    
    var _getCategories = function (cb) {

        $http({
            method: 'GET',
            url: '//thecatapi.com/api/categories/list',
            async: true,
        })
        .then(function (response) {
            cb(response);

        })
        .catch(function (data, status) {
            cb();
        });

    };

    return {
 
        getCategories: _getCategories
    }
}]);
