var app = angular.module('myApp', ['ngRoute']);

app.config(['$routeProvider', function ($routeProvider) {
    console.log("app");
    $routeProvider
        .when("/", {
            templateUrl: "scripts/views/categories.html"
        })
        .when("/category/:id", {
            templateUrl: "scripts/views/categoryDetail.html"
       
        }) .otherwise({
            redirectTo: '/'
        })
}]);
