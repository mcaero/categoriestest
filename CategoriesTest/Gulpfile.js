'use strict';

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    del = require('del'),
    less = require('gulp-less');


   
var tasksToRunDefault = ['clean', 'compileJS', 'less'];
//this usually goes into a different module so I can export them in case the gulpfile gets bigger
var config = {
    devScripts: [
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/bootstrap/dist/js/bootstrap.min.js',
        'bower_components/angular/angular.min.js',
        'bower_components/angular-route/angular-route.min.js',
      
        'app/scripts/main.js',
        'app/scripts/controllers/**/*.js',
        'app/scripts/services/**/*.js'
    ],
    lessFiles: [
        'Content/less/**/*.less'
    ]
}



gulp.task('clean', function () {
    console.log('deleting files in /app/dist output folder');
    return del(['./dist/**/*.*']);
});


gulp.task('compileJS', function () {
    console.log("js compiled");

    return gulp.src(config.devScripts)
      //.pipe(uglify().on('error', function () { util.log; }))
      .pipe(concat('scripts.min.js'))
      .pipe(gulp.dest('./dist/Scripts'));
});

//less task
gulp.task('less', function () {
    console.log("less compiled");
    return gulp.src(config.lessFiles)
        .pipe(less({ compress: true }))
        .pipe(concat('styles.min.css'))
        .pipe(gulp.dest('./dist/Less'));
});



gulp.task('watch', function() {
    var arrayTask = config.devScripts.concat(config.lessFiles);
  // Watch our scripts, and when they change run all tasks
    gulp.watch(arrayTask, tasksToRunDefault);
});

gulp.task('default', tasksToRunDefault.concat(['watch']), function () { });

